#!/usr/bin/env ruby

system("curl -X 'POST' \
  'http://localhost:8000/predict' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  \"model\": \"#{ARGV[0]}\",
  \"bill_length\": #{ARGV[1]},
  \"bill_depth\": #{ARGV[2]},
  \"flipper_length\": #{ARGV[3]},
  \"body_mass\": #{ARGV[4]}
}'")

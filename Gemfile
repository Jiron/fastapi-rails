source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

#ruby '2.7.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.1.1'
# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false

# Use Puma as the app server
gem 'puma', '~> 5.0'

gem 'msgpack', '~> 1.4.2'

#gem "libv8", github: "rubyjs/libv8", submodules: true
#gem 'therubyracer'
gem 'execjs'

# database
gem 'sqlite3'
#gem 'mysql2'
gem 'pg'
#gem 'ridgepole', github: 'winebarrel/ridgepole'
gem 'redis'
gem 'hiredis'
gem 'seed-fu'
gem 'seed_dump'
gem "breadcrumbs_on_rails"

# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 5.0'

# Assets
gem 'sass-rails', '>= 6'
gem 'bootstrap'
gem 'bootstrap-sass'
#gem 'font-awesome-sass'
gem 'jquery-rails'
gem 'uglifier'
#gem 'bootstrap_form', github: 'bootstrap-ruby/bootstrap_form'

# UI/UX
gem 'rails-i18n'
gem 'slim-rails'
gem 'kaminari'
gem 'gretel'
gem 'cocoon'

# Decorator
gem 'draper'

# Config
gem 'config'

# Authentication
gem 'sorcery'
gem 'omniauth'
gem 'omniauth-github'
gem 'bcrypt'

# AWS
gem 'aws-sdk', '~> 3'
gem 'aws-sdk-rails', '~> 3'

# JobQueue
gem 'resque'
gem 'resque-scheduler'
gem 'whenever', require: false

# Validator
gem 'validates_email_format_of'

# File Attachment
gem 'mini_magick', '>= 4.9.5'
gem 'carrierwave'
gem 'carrierwave-data-uri'
gem 'carrierwave-base64'
gem 'fog-aws'
gem 'aws-sdk-cloudfront'

# Model
gem 'enum_help'

# Serializer
gem 'active_model_serializers', '~> 0.10.0'

# Filter
gem 'ransack'

# SEO
gem 'meta-tags'
gem 'sitemap_generator'

# Exception Notifier
gem 'exception_notification'
gem 'slack-notifier'

# Date
gem 'business_time'
gem 'holiday_jp'

# Data
gem 'jp_prefecture'

group :development, :test, :staging do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'rspec-rails'
  gem 'rspec-request_describer'
  gem 'factory_bot_rails'
  gem 'rubocop', require: false
  gem 'rubocop-rails'
  gem 'faker'
  gem 'html2slim'
  gem 'pry'
  gem 'pry-byebug'
  gem 'pry-doc'
  gem 'pry-rails'
  gem 'pry-stack_explorer'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  #gem 'spring'
  #gem 'spring-commands-rspec'
  #gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'rails-erd'
  gem 'letter_opener_web', '~> 1.0'
  gem 'foreman'
  gem 'rails_best_practices', require: false
  gem 'slim_lint', require: false
  gem 'scss_lint', require: false

  # deploy
  gem 'aws-sdk-ec2'
  gem 'bcrypt_pbkdf'
  gem 'capistrano'
  gem 'capistrano-bundler'
  gem 'capistrano-rails'
  gem 'capistrano-rails-console'
  gem 'capistrano-rbenv'
  gem 'capistrano-resque', require: false
  gem 'capistrano-yarn'
  gem 'capistrano3-puma'
#  gem 'capistrano3-ridgepole'
  gem 'capistrano-nginx'
  gem 'ed25519'
end

group :test do
  gem 'capybara'
  gem 'webdrivers'
  gem 'show_me_the_cookies'
end

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'index#predict_species'

  get '/predict_species', to: 'index#predict_species'
  post '/predict', to: 'index#predict'
  get '/default', to: 'index#default'
end

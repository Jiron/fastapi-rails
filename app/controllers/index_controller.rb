require "uri"
require "net/http"
require 'json'

class IndexController < ApplicationController
  def predict_species
    @models = {'ランダムフォレスト': 'randomforest', '決定木': 'decisiontree', 'LightGBM': 'lightgbm',} #'ベースライン': 'bl', 'DNN': 'dnn', 'BoostedTrees': 'bt', 'DNNLinearCombined': 'dnnlc', 'BoostedTrees': 'bt', 'リニア': 'linear'}
    @data = PenguinItem.new

    render :predict_species
  end

  def predict
    logger.debug "debug: params=#{params.inspect}"
    #params = {}

    cmd = "#{Rails.root}/runner/curl_raw.sh #{params['data']['model']} #{params['data']['bill_length']} #{params['data']['bill_depth']} #{params['data']['flipper_length']} #{params['data']['body_mass']}"
    @result = JSON.parse(`#{cmd}`)

    render :predict
  end

  def default
    @headers = request.headers
  end

end

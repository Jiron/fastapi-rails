$(() => {
  const calcurator = new SaleItemCaluculator()

  $('tr.nested-fields').each(function(index){
    let numField = $(`#sale_sale_items_attributes_${index}_num`)
    let priceField = $(`#sale_sale_items_attributes_${index}_price`)
    let priceText = priceField.prev('p')
    let taxField = $(`#sale_sale_items_attributes_${index}_tax`)
    let taxText = taxField.prev('p')
    let totalField = $(`#sale_sale_items_attributes_${index}_total`)
    let totalText= totalField.prev('p')
    let productIdField = $(`#sale_sale_items_attributes_${index}_product_id`)
    let repairIdField = $(`#sale_sale_items_attributes_${index}_repair_id`)

    numField.on('change', function() {
        quantity = parseInt($(this).val())
        unitPrice = parseInt(priceField.val())
        tax = parseInt(taxField.val())
        total = (unitPrice + tax) * quantity
        totalField.val(total)
        totalText.text(total)
        calcurator.calc()
    })

    $(`#sale_sale_items_attributes_${index}__destroy`).next('a').on('click', function() {
      setTimeout(function(){
        calcurator.calc()
      }, 100);
    })

    productIdField.on('change', function() {
      id = $(this).val()
      products = $(this).parent().data('products')
      product = products.find((product) => product.id == id)
      priceField.val(product.gross)
      priceText.text(product.gross)
      taxField.val(product.gross * 0.1)
      taxText.text(product.gross * 0.1)
      quantity = parseInt(numField.val())
      unitPrice = parseInt(priceField.val())
      tax = parseInt(taxField.val())
      total = (unitPrice + tax) * quantity
      totalField.val(total)
      totalText.text(total)
      calcurator.calc()
    })

    repairIdField.on('change', function() {
      id = $(this).val()
      repairs = $(this).parent().data('repairs')
      repair = repairs.find((repair) => repair.id == id)
      priceField.val(repair.subtotal)
      priceText.text(repair.subtotal)
      taxField.val(repair.subtotal * 0.1)
      taxText.text(repair.subtotal * 0.1)
      quantity = parseInt(numField.val())
      unitPrice = parseInt(priceField.val())
      tax = parseInt(taxField.val())
      total = (unitPrice + tax) * quantity
      totalField.val(total)
      totalText.text(total)
      calcurator.calc()
    })
  })
})

class SaleItemCaluculator {
  constructor() {
    this.setSubTotalAmount()
    this.setTax()
    this.setTotalAmount()
    this.draw()
  }

  setSubTotalAmount() {
    let subTotal = 0;
    $('tr.nested-fields').each(function(index){
      if ($(this).css('display') == 'none') {
        return true
      }
      quantity = $(`#sale_sale_items_attributes_${index}_num`).val();
      unitPrice = $(`#sale_sale_items_attributes_${index}_price`).val();
      subTotal = subTotal + (quantity * unitPrice);
    })

    this.subTotalAmount = subTotal;
  }

  setTax() {
    let sumTax = 0;
    $('tr.nested-fields').each(function(index){
      if ($(this).css('display') == 'none') {
        return true
      }
      quantity = $(`#sale_sale_items_attributes_${index}_num`).val()
      tax = $(`#sale_sale_items_attributes_${index}_tax`).val()
      sumTax = sumTax + (quantity * tax)
    })
    this.tax = sumTax
  }

  setTotalAmount() {
    this.total = this.subTotalAmount + this.tax
  }

  calc() {
    this.setSubTotalAmount()
    this.setTax()
    this.setTotalAmount()
    this.draw()
  }

  draw() {
    $('p.ajax-subtotal').text(`小計：${this.subTotalAmount}`)
    $('p.ajax-tax').text(`消費税：${this.tax}`)
    $('p.ajax-total').text(`合計：${this.total}`)
  }
}

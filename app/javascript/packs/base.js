require("@rails/ujs").start()
require("channels")
require("jquery/dist/jquery.min")
window.jQuery = window.$ = $;

require("bootstrap/dist/js/bootstrap.min")
require("bootstrap/dist/js/bootstrap.bundle.min")
require("admin-lte/dist/js/adminlte.min")
require("infinite-scroll/js/index")
require("ekko-lightbox/dist/ekko-lightbox.min")
require('./jquery.jpostal')
require('./modal')
require('./preview')
import "cocoon"

$(() => {
  $(document).on('change', '.auto-submit-select', (e) => {
    e.currentTarget.form.submit()
  })
  if ($('a[rel=next]').length > 0) {
    $('.infinitescroll').infiniteScroll({
        path: 'a[rel=next]',
        append: '.post',
        history: false,
        prefill: true,
        status: '.page-load-status',
    })
  }
})

$(window).ready(() =>{
  if ($('#postal-code').length <= 0) {
    return
  }
  $('#postal-code').jpostal({   
    click : '.postal-btn',
    postcode : ['#postal-code'],
    address : {
      '#prefecture': '%3',
      '#address'  : '%4%5'
    }
  })
})

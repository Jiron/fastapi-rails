$(() => {
  const calcurator = new RepairCalculator()
  // モーダル内の商品を非表示
  $('#parts-modal tr').each(function () {
    $(this).hide()
  })
  // discount type (値引き)
  $('input[name="repair_form[discount_type]"]').change(function() {
    calcurator.setDiscountPrice()
  })
  // special discount type (割引き)
  $('input[name="repair_form[special_discount_type]"]').change(function () {
    // 割引率を元に合計の再計算を行う
    calcurator.calc()
  })
  // adjust type (調整費用)
  $('input[name="repair_form[adjust_type]"]').change(function() {
    calcurator.setAdjustPrice()
  })
  // special adjust price (特別調整)
  $('input[name="repair_form[special_adjust_price]"]').blur(function() {
    calcurator.setSpecialAdjustPrice()
  })

  $('.js-filter-products input[type="radio"]').click(function() {
    const modelId = $(this).data('model-id')
    const $records = $(this).closest('.js-filter-products').find('tr')
    $records.each(function() {
      const modelIds = $(this).data('model-ids')
      if (modelIds.includes(modelId)) {
        $(this).show()
      } else {
        $(this).hide()
      }
    })
  })

  // 修理追加
  $('.add-repair').click(function() {
    const type = $(this).data('type')
    // 選択行をコピー
    const $el =  $(this).closest('tr').clone(true)
    // 末尾のtd(+ボタン)のみ削除
    $el.children('td:last').remove()
    // 対象テーブルに追加
    $(`#${type}-table`).append($el)
    // 金額の再計算
    if (type === 'parts') {
      calcurator.setPartsTotal()
    }
  })

  // 送信
  $('#repair-form').submit(function() {
    const $form = $(this)
    const $products_tr = $('#parts-table tr')
    if ($products_tr.length === 0) {
      alert('パーツを追加してください')
      return false
    }
    // パーツ・アクセサリ一覧をhidden fieldに追加
    $('#parts-table tr').each(function() {
      const id = $(this).data('id')
      const $el = $('<input type="hidden" name="repair_form[parts_ids][]" value="' + id + '">')
      $form.append($el)
    })
  })

  // パーツリセット処理
  $("#reset-parts").click(function(event){
    event.preventDefault();
    $('#parts-table tr').each(function () {
      $(this).remove();
      calcurator.setPartsTotal();
    })
  });
})

class RepairCalculator {
  subtotal = 0
  total = 0
  tax = 0
  partsSubtotal = 0
  discountTotalPrice = 0

  _partsTotal = 0
  _discountPrice = 0
  _specialDiscountPrice = 0
  _adjustPrice = 0
  _specialAdjustPrice = 0

  constructor() {
    this.setPartsTotal()
    this.setDiscountPrice()
    this.setAdjustPrice()
    this.setSpecialAdjustPrice()
    this.calc()
  }

  getPartsTotal() {
    let total = 0
    $('#parts-table tr').each(function() {
      total += $(this).data('price')
    })
    return total
  }

  setPartsTotal() {
    this._partsTotal = this.getPartsTotal()
    $('#el-parts-total').text(this._partsTotal)
    this.calc()
  }

  getDiscountPriceByType() {
    let price = 0
    const type = $('input[name="repair_form[discount_type]"]:checked').val()
    switch (type) {
      case 'two':
        price = -1000
        break
      case 'three':
        price = -2000
        break
      case 'four':
        price = -3000
        break
      case 'five':
        price = -4000
        break
      case 'campaign':
        price = -500
        break
    }
    this.discountTotalPrice = price + this._specialDiscountPrice
    $('#el-discount-total').text(this.discountTotalPrice)
    return price
  }

  setDiscountPrice() {
    this._discountPrice = this.getDiscountPriceByType()
    $('#el-discount-price').text(this._discountPrice)
    this.calc()
  }

  getSpecialDiscountPriceByType() {
    let price = 0
    const productTotal = this._partsTotal
    const type = $('input[name="repair_form[special_discount_type]"]:checked').val()
    switch (type) {
      case 'repeater_discounted':
        price = (productTotal > 0) ? -Math.ceil(productTotal * 0.05) : 0
        break
      case 'special_discounted':
        price = (productTotal > 0) ? -Math.ceil(productTotal * 0.1) : 0
        break
    }
    this.discountTotalPrice = price + this._discountPrice
    $('#el-discount-total').text(this.discountTotalPrice)
    return price
  }

  setSpecialDiscountPrice() {
    this._specialDiscountPrice = this.getSpecialDiscountPriceByType()
    $('#el-special-discount-price').text(this._specialDiscountPrice)
  }

  getAdjustPriceByType() {
    let price = 0
    const type = $('input[name="repair_form[adjust_type]"]:checked').val()
    switch (type) {
      case 'level1':
        price = 1000
        break
      case 'level2':
        price = 2000
        break
      case 'level3':
        price = 3000
        break
      case 'level4':
        price = 4000
        break
      case 'level5':
        price = 4980
        break
      case 'level6':
        price = 6980
        break
      case 'level7':
        price = 12800
        break
      case 'level8':
        price = 16800
        break
    }
    return price
  }

  setAdjustPrice() {
    this._adjustPrice = this.getAdjustPriceByType()
    $('#el-adjust-price').text(this._adjustPrice)
    this.calc()
  }

  setSpecialAdjustPrice() {
    this._specialAdjustPrice = parseInt($('input[name="repair_form[special_adjust_price]"]').val())
    $('#el-special-adjust-price').text(this._specialAdjustPrice)
    this.calc()
  }

  calc() {
    this.setSpecialDiscountPrice()
    this.calcSubtotal()
    this.calcTax()
    this.calcTotal()
    this.calcPartsSubtotal()
    this.draw()
  }

  calcSubtotal() {
    this.subtotal = this._partsTotal
        + this._adjustPrice
        + this._specialAdjustPrice
        + this.discountTotalPrice
  }

  calcPartsSubtotal() {
    this.partsSubtotal = this._partsTotal
        + this._adjustPrice
        + this._specialAdjustPrice
  }

  calcTax() {
    const TAX_RATE = 0.1
    this.tax = this.subtotal > 0 ? Math.ceil(this.subtotal * TAX_RATE) : 0
  }

  calcTotal() {
    this.total = this.subtotal + this.tax
  }

  draw() {
    $('#el-subtotal').text(this.subtotal)
    $('#el-repair-subtotal').text(this.partsSubtotal)
    $('#el-tax').text(this.tax)
    $('#el-total').text(this.total)
  }
}

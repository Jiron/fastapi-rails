import Vue from 'vue'
import App from '../components/new_order'

document.addEventListener('DOMContentLoaded', () => {
  const el = document.getElementById('new-order')
  const partsTypes = JSON.parse(el.dataset.parts_types)
  const targetModels = JSON.parse(el.dataset.target_models)
  const availableProducts = JSON.parse(el.dataset.available_products)

  new Vue({
    el: '#new-order',
    render: h => h(App, {
      props: {
        partsTypes: partsTypes,
        targetModels: targetModels,
        availableProducts: availableProducts
      }
    })
  })
})

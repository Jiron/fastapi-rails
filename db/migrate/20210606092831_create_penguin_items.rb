class CreatePenguinItems < ActiveRecord::Migration[6.1]
  def change
    create_table :penguin_items do |t|
      #t.timestamps

      t.string :model, default: 'randomforest', null: false
      t.float :bill_length, default: 39.1, null: false
      t.float :bill_depth, default: 18.7, null: false
      t.float :flipper_length, default: 181, null: false
      t.float :body_mass, default: 3750, null: false
    end
  end
end
